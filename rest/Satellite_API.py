from REST import REST

class Satellite_API(object):
	"""
	This class implements the Red Hat Satellite 6 REST API.

	The naming convetion used for the methods converts the URI to a name
	by replacing / with _ and then appending the type of call (e.g. get).
	"""

	#---------------------------------------------------------------------------
	#	Initializer
	#---------------------------------------------------------------------------
	def __init__(self, base_url, username, password):
		self._REST = REST(input_base_url=base_url, username=username, password=password)


	#---------------------------------------------------------------------------
	#	Overhead Methods
	#---------------------------------------------------------------------------
	def get_data(self):
		data = {
			'per_page': 1000
		}
		return(data)

	def get_headers(self):
		headers = {
			'Content-Type': 'application/json',
		}
		return(headers)


	#---------------------------------------------------------------------------
	#	Activation Keys
	#---------------------------------------------------------------------------
	def katello_api_activation_keys_content_override_put(self, activation_key_id, content_label, value, name, remove):
		headers = self.get_headers()
		data = self.get_data()
		data['content_overrides'] = [{
			"content_label": content_label,
			"value": value,
			"name": name,
			"remove": remove
		}]
		uri = "/katello/api/activation_keys/" + str(activation_key_id) + "/content_override"
		response = self._REST.put(uri, headers=headers, data=data)
		return(response)

	def katello_api_activation_keys_delete(self, activation_key_id):
		uri = "/katello/api/activation_keys/" + str(activation_key_id)
		response = self._REST.delete(uri)
		return(response)

	def katello_api_activation_keys_post(self, organization_id, activation_key_name, environment_id, content_view_id, description="", unlimited_hosts=True, service_level="Premium", auto_attach=True, purpose_usage="", purpose_role="", purpose_addons=[]):
		headers = self.get_headers()
		data = self.get_data()
		data['organization_id'] = organization_id
		data['name'] = activation_key_name
		data['environment_id'] = environment_id
		data['content_view_id'] = content_view_id
		data['description'] = description
		data['unlimited_hosts'] = unlimited_hosts
		data['service_level'] = service_level
		data['auto_attach'] = auto_attach
		data['purpose_usage'] = purpose_usage
		data['purpose_role'] = purpose_role
		data['purpose_addons'] = purpose_addons
		uri = "/katello/api/activation_keys"
		response = self._REST.post(uri, headers=headers, data=data)
		return(response)

	def katello_api_organizations_activation_keys_get(self, organization_id):
		headers = self.get_headers()
		uri = "/katello/api/organizations/" + str(organization_id) + "/activation_keys"
		response = self._REST.get(uri, headers=headers)
		return(response)


	#---------------------------------------------------------------------------
	#	Component Content Views
	#---------------------------------------------------------------------------
	def katello_api_content_views_publish_post(self, content_view_id, description=''):
		headers = self.get_headers()
		data = self.get_data()
		data['description'] = description
		uri = "/katello/api/content_views/" + str(content_view_id) + "/publish"
		response = self._REST.post(uri, headers=headers, data=data)
		return(response)
		
	def katello_api_organizations_content_views_get(self, organization_id):
		headers = self.get_headers()
		uri = "/katello/api/organizations/" + str(organization_id) + "/content_views"
		response = self._REST.get(uri, headers=headers)
		return(response)

	def katello_api_organizations_content_views_post(self, name, label,
		organization_id, composite=False, description='', repository_ids=[],
		component_ids=[]):
		headers = self.get_headers()
		data = self.get_data()
		data['composite'] = composite
		data['description'] = ''
		data['label'] = label
		data['name'] = name
		data['repository_ids'] = repository_ids
		uri = "/katello/api/organizations/" + str(organization_id) + "/content_views"
		response = self._REST.post(uri, headers=headers, data=data)
		return(response)


	#---------------------------------------------------------------------------
	#	Composite Content Views
	#---------------------------------------------------------------------------
	def katello_api_content_view_versions_promote_post(self, content_view_id, environment_ids, description=''):
		headers = self.get_headers()
		data = self.get_data()
		data['description'] = description
		data['environment_ids'] = environment_ids
		uri = "/katello/api/content_view_versions/" + str(content_view_id) + "/promote"
		response = self._REST.post(uri, headers=headers, data=data)
		return(response)

	def katello_api_content_views_content_view_components_add_put(self, composite_content_view_id, components):
		headers = self.get_headers()
		data = self.get_data()
		data['components'] = []
		elements = components.keys()
		for element in elements:
			element = {
				"content_view_version_id": str(components[element]),
				"content_view_id": str(element)
			}
			data["components"].append(element)
		uri = "/katello/api/content_views/" + str(composite_content_view_id) + "/content_view_components/add"
		response = self._REST.put(uri, headers=headers, data=data)
		return(response)

	def katello_api_content_views_content_view_components_get(self, composite_content_view_id):
		headers = self.get_headers()
		uri = "/katello/api/content_views/" + str(composite_content_view_id) + "/content_view_components"
		response = self._REST.get(uri, headers=headers)
		return(response)

	def katello_api_content_views_content_view_components_put(self, composite_content_view_id, component_id, component_version_id):
		headers = self.get_headers()
		uri = "/katello/api/content_views/" + str(composite_content_view_id) + "/content_view_components" + "/" + str(component_id)
		data = self.get_data()
		data['content_view_version_id'] = component_version_id
		response = self._REST.put(uri, headers=headers, data=data)
		return(response)

	#---------------------------------------------------------------------------
	#	Content View Versions
	#---------------------------------------------------------------------------
	def katello_api_content_views_content_view_versions_get(self, organization_id, content_view_id):
		headers = self.get_headers()
		uri = "/katello/api/content_views/" + str(content_view_id) + "/content_view_versions"
		response = self._REST.get(uri, headers=headers)
		return(response)


	#---------------------------------------------------------------------------
	#	Foreman Tasks
	#---------------------------------------------------------------------------
	def foreman_tasks_api_tasks(self, task_id):
		headers = self.get_headers()
		uri = "/foreman_tasks/api/tasks/" + str(task_id)
		response = self._REST.get(uri, headers=headers)
		return(response)


	#---------------------------------------------------------------------------
	#	Lifecycle Environments
	#---------------------------------------------------------------------------
	def katello_api_organizations_environments_get(self, organization_id):
		uri = "/katello/api/organizations/" + str(organization_id) + "/environments"
		response = self._REST.get(uri)
		return(response)

	def katello_api_organizations_environments_post(self, organization_id, name, label, prior_id, description=''):
		headers = self.get_headers()
		data = self.get_data()
		data['name'] = name
		data['label'] = label
		data['description'] = description
		data['prior_id'] = prior_id
		uri = "/katello/api/organizations/" + str(organization_id) + "/environments"
		response = self._REST.post(uri, headers=headers, data=data)
		return(response)


	#---------------------------------------------------------------------------
	#	Locations
	#---------------------------------------------------------------------------
	def api_locations_delete(self, id):
		uri = '/api/locations/' + str(id) + '/'
		response = self._REST.delete(uri)
		return(response)

	def api_locations_get(self, id=''):
		uri = '/api/locations/'
		if (id):
			uri += id + "/"
		response = self._REST.get(uri)
		return(response)

	def api_locations_post(self, name, description=''):
		headers = self.get_headers()
		data = self.get_data()
		data['name'] = name
		data['description'] = description
		uri = '/api/locations/'
		response = self._REST.post(uri, headers=headers, data=data)
		return(response)


	#---------------------------------------------------------------------------
	#	Organizations
	#---------------------------------------------------------------------------
	def katello_api_organizations_delete(self, organization_id):
		uri = '/katello/api/organizations/' + str(organization_id) + '/'
		response = self._REST.delete(uri)
		return(response)

	def katello_api_organizations_get(self, organization_id=''):
		uri = '/katello/api/organizations/'
		if (organization_id):
			uri += organization_id + "/"
		response = self._REST.get(uri)
		return(response)

	def katello_api_organizations_post(self, name, description=''):
		headers = self.get_headers()
		data = self.get_data()
		data['name'] = name
		data['description'] = description
		uri = '/katello/api/organizations/'
		response = self._REST.post(uri, headers=headers, data=data)
		return(response)


	#---------------------------------------------------------------------------
	#	Products
	#---------------------------------------------------------------------------
	def katello_api_organizations_products_get(self, organization_id):
		headers = self.get_headers()
		data = self.get_data()
		data['organization_id'] = organization_id
		uri = "/katello/api/organizations/" + str(organization_id) + "/products"
		response = self._REST.get(uri, headers=headers, data=data)
		return(response)


	#---------------------------------------------------------------------------
	#	Repositories
	#---------------------------------------------------------------------------
	def katello_api_organizations_repositories_get(self, organization_id):
		headers = self.get_headers()
		uri = "/katello/api/organizations/" + str(organization_id) + "/repositories"
		response = self._REST.get(uri, headers=headers)
		return(response)


	#---------------------------------------------------------------------------
	#	Repository Sets
	#---------------------------------------------------------------------------
	def katello_api_products_repository_sets_enable_put(self, organization_id, product_id, repository_id, releasever):
		headers = self.get_headers()
		data = self.get_data()
		data['releasever'] = releasever
		uri = "/katello/api/products/" + str(product_id) + "/repository_sets/" + str(repository_id) + "/enable"
		response = self._REST.put(uri, headers=headers, data=data)
		return(response)

	def katello_api_products_repository_sets_get(self, organization_id, product_id):
		headers = self.get_headers()
		data = self.get_data()
		data['organization_id'] = organization_id
		uri = "/katello/api/products/" + str(product_id) + "/repository_sets"
		response = self._REST.get(uri, headers=headers, data=data)
		return(response)


	#---------------------------------------------------------------------------
	#	Subscriptions
	#---------------------------------------------------------------------------
	def katello_api_organizations_subscriptions_get(self, organization_id):
		uri = "/katello/api/organizations/" + str(organization_id) + "/subscriptions"
		response = self._REST.get(uri)
		return(response)

	def katello_api_organizations_subscriptions_refresh_manifest_put(self, organization_id):
		headers = self.get_headers()
		uri = "/katello/api/organizations/" + str(organization_id) + "/subscriptions/refresh_manifest"
		response = self._REST.put(uri, headers=headers)
		return(response)

	def katello_api_organizations_subscriptions_upload_post(self,
		organization_id, manifest_file_name):
		uri = "/katello/api/organizations/" + str(organization_id) + "/subscriptions/upload"
		manifest_file = open(manifest_file_name, 'rb')
		files = {
			'content': manifest_file
		}
		response = self._REST.post(uri, files=files)
		return(response)


	#---------------------------------------------------------------------------
	#	Sync PLans
	#---------------------------------------------------------------------------
	def katello_api_organizations_sync_plans_add_products_put(self, organization_id, sync_plan_id, product_ids):
		headers = self.get_headers()
		data = self.get_data()
		data['product_ids'] = product_ids
		uri = "/katello/api/organizations/" + str(organization_id) + "/sync_plans/" + str(sync_plan_id) + "/add_products"
		response = self._REST.put(uri, headers=headers, data=data)
		return(response)

	def katello_api_organizations_sync_plans_get(self, organization_id):
		uri = "/katello/api/organizations/" + str(organization_id) + "/sync_plans"
		response = self._REST.get(uri)
		return(response)

	def katello_api_organizations_sync_plans_post(self, organization_id, sync_plan_name, interval, sync_date_time):
		headers = self.get_headers()
		data = self.get_data()
		data['name'] = sync_plan_name
		data['interval'] = interval
		data['sync_date'] = sync_date_time
		data['enabled'] = True
		uri = "/katello/api/organizations/" + str(organization_id) + "/sync_plans"
		response = self._REST.post(uri, headers=headers, data=data)
		return(response)


	#---------------------------------------------------------------------------
	#	Support Methods
	#---------------------------------------------------------------------------
	def __str__(self):
		output = ""
		output += "Satellite_API Object\n"
		output += "--------------------\n"
		output += str(self._REST)
		output += "\n"
		return(output)

