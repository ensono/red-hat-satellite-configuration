#! /usr/bin/env python

# standard modules
import argparse
import getpass
import sys

# custom modules
from Satellite import Satellite
import RestExceptions

def main():
	# get the parameters
	args = usage()

	# initialize the variables
	base_url = args.base_url
	organization_name = args.organization_name
	composite_content_view_name = args.composite_content_view_name
	target_lifecycle_environment = args.target_lifecycle_environment

	# get the user's credentials
	username = raw_input('User Name: ')
	password = getpass.getpass(prompt='Password: ')

	# instantiate a Satellite object
	satellite = Satellite(base_url, username, password)

	print("get lifecycle environments")
	lifecycle_environments = satellite.get_lifecycle_environments(organization_name)
	if (target_lifecycle_environment == lifecycle_environments[0]):
		# promoting to Library is not permitted
		print("ERROR: promoting to the " + lifecycle_environments[0] + " lifecycle environment is not permitted")
	elif (target_lifecycle_environment == lifecycle_environments[1]):
		# promoting to the first lifecycle environment requires
		# publishing new versions of the content views first
		print("get a list of component content views")
		component_content_views = satellite.list_composite_content_view_components(organization_name, composite_content_view_name)
		for component_content_view in component_content_views:
			print("publish a new version of the " + component_content_view + " component content view")
			response = satellite.publish_content_view(organization_name, component_content_view, only_if_new=False)
			print("update the version of the " + component_content_view + " component content view in the " + composite_content_view_name + " composite content view to the latest")
			response = satellite.update_component_content_view_to_latest_version(organization_name, composite_content_view_name, component_content_view)
		print("publish a new version of the composite content view " + composite_content_view_name)
		response = satellite.publish_content_view(organization_name, composite_content_view_name, only_if_new=False)
		print("promote the " + composite_content_view_name + " composite content view to the " + lifecycle_environments[1] + " lifecycle environment")
		response = satellite.promote_latest_content_view_version_to_lifecycle_environment(organization_name, composite_content_view_name, target_lifecycle_environment)
	else:
		print("promote the composite content view to the next lifecycle environment")
		response = satellite.promote_latest_content_view_version_to_lifecycle_environment(organization_name, composite_content_view_name, target_lifecycle_environment)
	
	sys.exit(0)


def usage():
	parser = argparse.ArgumentParser()
	parser.add_argument("-u", "--base-url", dest="base_url", default="https://dglpsat01.int.ensono.com", help="base URL of the Satellite server (e.g.  https://dglpsat01.int.ensono.com)")
	parser.add_argument("-o", "--organization-name", dest="organization_name", default="", help="Organization name")
	parser.add_argument("-v", "--composite-content-view-name", dest="composite_content_view_name", default="", help="Composite Content View name")
	parser.add_argument("-e", "--target-lifecycle-environment", dest="target_lifecycle_environment", default="", help="target Lifecycle Environment")
	args = parser.parse_args()
	return (args)


if __name__ == "__main__":
	main()

