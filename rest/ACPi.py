#! /usr/bin/env python

#-------------------------------------------------------------------------------
#	Issues
#
#-------------------------------------------------------------------------------

# standard modules
import argparse
import getpass
import json
import pprint
import sys
import time

from Satellite import Satellite
import RestExceptions

def list_products(response):
	products = response.json()['results']
	for product in products:
		print(product['name'])

def list_repository_sets(response):
	repository_sets = response.json()['results']
	for repository_set in repository_sets:
		print(repository_set['id'] + ": " + repository_set['name'])

def display_response_and_wait(response):
	print(response)
	if (response):
		pprint.pprint(response.json())
	raw_input("Press Enter to continue ...")


def main():
	# get any parameters
#   args = usage()

	# get the user's credentials
	base_url = 'https://dglpsat01.int.ensono.com'
	# override
	# username = raw_input('User Name: ')
	# password = getpass.getpass(prompt='Password: ')
	username = "admin"
	password = "Welcome1!"

	# instantiate a Satellite object
	satellite = Satellite(base_url, username, password)

	# constants
	organization_name = 'ACPi'
	location_name = 'Conway'
	manifest_file_name = 'manifest_ACPi_20210825T130438Z.zip'
	product_name = 'Red Hat Enterprise Linux for x86_64'
	sync_plan_name = 'ACPi 10:00PM UTC'
	activation_key_name_1 = 'RHEL 8 Development'
	activation_key_name_2 = 'RHEL 8 Test'
	activation_key_name_3 = 'RHEL 8 Production'
	environment_name = 'Development'
	content_view_name = 'RHEL 8 Server'

	try:
		# Organizations
		# print("list organizations")
		# display_response_and_wait(satellite.list_organizations())
		### print("create organization")
		### satellite.create_organization(organization_name)
		# print("list organizations")
		# display_response_and_wait(satellite.list_organizations())
		# print("delete organization")
		# display_response_and_wait(satellite.delete_organization(organization_name))
		# print("list organizations")
		# display_response_and_wait(satellite.list_organizations())
		# print("create organization")
		# display_response_and_wait(satellite.create_organization(organization_name))
		# print("list organizations")
		# display_response_and_wait(satellite.list_organizations())

		# Locations
		# print("list locations")
		# display_response_and_wait(satellite.list_locations())
		### print("create location")
		### satellite.create_location(location_name)
		# print("list locations")
		# display_response_and_wait(satellite.list_locations())
		# print("delete location")
		# display_response_and_wait(satellite.delete_location(location_name))
		# print("list locations")
		# display_response_and_wait(satellite.list_locations())
		# print("create location")
		# display_response_and_wait(satellite.create_location(location_name))
		# print("list locations")
		# display_response_and_wait(satellite.list_locations())

		# Subscriptions
		# print("list subscriptions")
		# display_response_and_wait(satellite.list_subscriptions(organization_name))
		### print("load subscriptions")
		### satellite.upload_subscription_manifest(organization_name, manifest_file_name)
		# print("list subscriptions")
		# display_response_and_wait(satellite.list_subscriptions(organization_name))
		### print("refresh subscriptions")
		### satellite.refresh_subscription_manifest(organization_name)
		# print("list subscriptions")
		# display_response_and_wait(satellite.list_subscriptions(organization_name))

		# Products
		# display_response_and_wait(satellite.list_products(organization_name))
		# print("list products")
		# response = satellite.list_products(organization_name)
		# list_products(response)
		# print("list RHEL product ID")
		# product_id = satellite.get_product_id(organization_name, product_name)
		# print("Name: " + product_name + "\n  ID: " + str(product_id))

		# Repository Sets
		# print("list repository sets")
		# response = satellite.list_repository_sets(organization_name, product_name)
		# list_repository_sets(response)
		# print("list specific repository set IDs")
		repository_set_name_1 = 'Red Hat Enterprise Linux 8 for x86_64 - AppStream (RPMs)'
		# repository_set_id = satellite.get_repository_set_id(organization_name, product_name, repository_set_name_1)
		# print("Repository Set Name: " + repository_set_name_1 + "\n Repository Set ID: " + repository_set_id)
		repository_set_name_2 = 'Red Hat Enterprise Linux 8 for x86_64 - BaseOS (RPMs)'
		# repository_set_id = satellite.get_repository_set_id(organization_name, product_name, repository_set_name_2)
		# print("Repository Set Name: " + repository_set_name_2 + "\n Repository Set ID: " + repository_set_id)
		repository_set_name_3 = 'Red Hat Satellite Tools 6.9 for RHEL 8 x86_64 (RPMs)'
		# repository_set_id = satellite.get_repository_set_id(organization_name, product_name, repository_set_name_3)
		# print("Repository Set Name: " + repository_set_name_3 + "\n Repository Set ID: " + repository_set_id)
		releasever = "8"
		### print("enable RHEL 8 AppStream")
		### satellite.enable_repository(organization_name, product_name, repository_set_name_1, releasever)
		### print("enable RHEL 8 BaseOS")
		### satellite.enable_repository(organization_name, product_name, repository_set_name_2, releasever)
		### print("enable Satellite Tools for RHEL 8")
		### satellite.enable_repository(organization_name, product_name, repository_set_name_3, releasever)

		# Sync Plans
		# European date format and UTC
		### print("create a sync plan")
		### satellite.create_sync_plan(organization_name, sync_plan_name, 'daily', '10/08/2021 12:35 PM')
		# print("list sync plans")
		# display_response_and_wait(satellite.list_sync_plans(organization_name))
		# print("list sync plan ID")
		# sync_plan_id = satellite.get_sync_plan_id(organization_name, sync_plan_name)
		# print("Sync Plan ID: " + str(sync_plan_id))

		# Products
		product_names = [product_name]
		### print("add product to sync plan")
		### satellite.add_products_to_sync_plan(organization_name, sync_plan_name, product_names)

		#-----------------------------------------------------------------------
		#	Allow the sync plan to run.
		#-----------------------------------------------------------------------

		# Repositories
		# print("list repositories")
		# display_response_and_wait(satellite.list_repositories(organization_name))
		repository_name_1 = 'Red Hat Enterprise Linux 8 for x86_64 - AppStream RPMs 8'
		repository_name_2 = 'Red Hat Enterprise Linux 8 for x86_64 - BaseOS RPMs 8'
		repository_names_1 = [repository_name_1, repository_name_2]
		repository_name_3 = 'Red Hat Satellite Tools 6.9 for RHEL 8 x86_64 RPMs'
		repository_names_2 = [repository_name_3]

		# Component Content Views - create
		# print("list content views")
		# display_response_and_wait(satellite.list_content_views(organization_name))
		print("create RHEL 8 content view")
		satellite.create_content_view('RHEL 8', 'RHEL_8', organization_name, composite=False, repository_names=repository_names_1)
		# print("list content ID")
		# content_view_id = satellite.get_content_view_id(organization_name, 'RHEL 8')
		# print("RHEL 8 Content View ID: " + str(content_view_id))
		print("create Satellite Tools RHEL 8 content view")
		satellite.create_content_view('Satellite Tools RHEL 8', 'Satellite_Tools_RHEL_8', organization_name, composite=False, repository_names=repository_names_2)
		# print("list content ID")
		# content_view_id = satellite.get_content_view_id(organization_name, 'Satellite Tools RHEL 8')
		# print("Satellite Tools RHEL 8 Content View ID: " + str(content_view_id))

		# Component Content Views - publish
		print("publish the satellite tools content view")
		satellite.publish_content_view(organization_name, 'Satellite Tools RHEL 8')
		print("publish the RHEL 8 content view")
		satellite.publish_content_view(organization_name, 'RHEL 8')

		# Component Content Views - list versions
		# print("list RHEL 8 content view versions")
		# display_response_and_wait(satellite.list_content_view_versions(organization_name, 'RHEL 8'))
		# print("list Satellite Tools RHEL 8 content view versions")
		# display_response_and_wait(satellite.list_content_view_versions(organization_name, 'Satellite Tools RHEL 8'))

		# Composite Content View - create
		print("create RHEL 8 Server composite content view")
		satellite.create_content_view('RHEL 8 Server', 'RHEL_8_Server', organization_name, composite=True)
		# print("list content views")
		# display_response_and_wait(satellite.list_content_views(organization_name))
		# print("list composite content view components")
		# display_response_and_wait(satellite.list_composite_content_view_components(organization_name, content_view_name))
		component_names = ['RHEL 8', 'Satellite Tools RHEL 8']
		print("add components to composite content view")
		satellite.add_components_to_composite_content_view(organization_name, 'RHEL 8 Server', component_names)
		# print("list composite content view components")
		# content_view_components = satellite.list_composite_content_view_components(organization_name, 'RHEL 8 Server')
		# for content_view_component in content_view_components:
		# 	print("    " + content_view_component)

		# Composite Content View - publish
		print("publish RHEL 8 Server composite content view")
		satellite.publish_content_view(organization_name, 'RHEL 8 Server')

		# Lifecycle Environments
		# print("list lifecycle environments")
		# display_response_and_wait(satellite.list_lifecycle_environments(organization_name))
		# print("list lifecycle environment ID")
		# lifecycle_environment_id = satellite.get_lifecycle_environment_id(organization_name, 'Library')
		# print("Lifecycle Environment ID: " + str(lifecycle_environment_id))
		print("create lifecycle environment Development")
		satellite.create_lifecycle_environment(organization_name, 'Development', 'Development', 'Library')
		print("create lifecycle environment Test")
		satellite.create_lifecycle_environment(organization_name, 'Test', 'Test', 'Development')
		print("create lifecycle environment Production")
		satellite.create_lifecycle_environment(organization_name, 'Production', 'Production', 'Test')
		# print("list lifecycle environments")
		# display_response_and_wait(satellite.list_lifecycle_environments(organization_name))
		# print("list lifecycle environment versions")
		# display_response_and_wait(satellite.list_content_view_versions(organization_name, 'RHEL 8 Server'))
		# print("list latest lifecycle environment version ID")
		# content_view_version_id = satellite.get_latest_content_view_version_id(organization_name, 'RHEL 8 Server')
		# print("Content View version ID: " + str(content_view_version_id))
		print("promote latest composite content view version to Development")
		satellite.promote_latest_content_view_version_to_lifecycle_environment(organization_name, 'RHEL 8 Server', 'Development')
		print("promote latest composite content view version to Test")
		satellite.promote_latest_content_view_version_to_lifecycle_environment(organization_name, 'RHEL 8 Server', 'Test')
		print("promote latest composite content view version to Production")
		satellite.promote_latest_content_view_version_to_lifecycle_environment(organization_name, 'RHEL 8 Server', 'Production')

		# Activation Keys
		# print("list activation keys")
		# display_response_and_wait(satellite.list_activation_keys(organization_name))
		# print("delete activation key")
		# display_response_and_wait(satellite.delete_activation_key(organization_name, activation_key_name))
		# print("list activation keys")
		# display_response_and_wait(satellite.list_activation_keys(organization_name))
		print("create a Development activation key")
		satellite.create_activation_key(organization_name, activation_key_name_1, "Development", "RHEL 8 Server")
		print("create a Test activation key")
		satellite.create_activation_key(organization_name, activation_key_name_2, "Test", "RHEL 8 Server")
		print("create a Production activation key")
		satellite.create_activation_key(organization_name, activation_key_name_3, "Production", "RHEL 8 Server")
		# print("list activation keys")
		# display_response_and_wait(satellite.list_activation_keys(organization_name))
		print("enable the Satellite Tools content to the Development key")
		satellite.override_activation_key_content(organization_name, activation_key_name_1, "satellite-tools-6.9-for-rhel-8-x86_64-rpms", True, "enabled")
		print("enable the Satellite Tools content to the Test key")
		satellite.override_activation_key_content(organization_name, activation_key_name_2, "satellite-tools-6.9-for-rhel-8-x86_64-rpms", True, "enabled")
		print("enable the Satellite Tools content to the Production key")
		satellite.override_activation_key_content(organization_name, activation_key_name_3, "satellite-tools-6.9-for-rhel-8-x86_64-rpms", True, "enabled")

		sys.exit(0)

	except RestExceptions.RestError as e:
		print(e)
		sys.exit(1)

	sys.exit(0)


if __name__ == "__main__":
    main()

