import sys
import time

from Satellite_API import Satellite_API

class Satellite(object):
	"""
	This class provides an abstraction for the REST API.  The goal
	was to provide useful functions that have basic interfaces so that
	the REST API layer can change without impacting scripts using this
	layer.
	"""

	def __init__(self, base_url, username, password):
		self._Satellite_API = Satellite_API(base_url, username, password)
		self._organization_ids = {}

	#---------------------------------------------------------------------------
	#	Activation Keys
	#---------------------------------------------------------------------------
	def activation_key_exists(self, organization_name, activation_key_name):
		response = self.list_activation_keys(organization_name)
		activation_keys = response.json()['results']
		exists = False
		for activation_key in activation_keys:
			if (activation_key_name == activation_key['name']):
				exists = True
				break
		return(exists)

	def create_activation_key(self, organization_name, activation_key_name, environment_name, content_view_name, description=""):
		return_code = False
		if not self.activation_key_exists(organization_name, activation_key_name):
			print("activation key " + activation_key_name + " does not exist, creating ...")
			organization_id = self.get_organization_id(organization_name)
			environment_id = self.get_lifecycle_environment_id(organization_name, environment_name)
			content_view_id = self.get_content_view_id(organization_name, content_view_name)
			response = self._Satellite_API.katello_api_activation_keys_post(organization_id, activation_key_name, environment_id, content_view_id, description)

			response = self.wait_for_completion(response)
			return_code = True
		return(return_code)

	def delete_activation_key(self, organization_name, activation_key_name):
		activation_key_id = self.get_activation_key_id(organization_name, activation_key_name)
		response = self._Satellite_API.katello_api_activation_keys_delete(activation_key_id)
		response = self.wait_for_completion(response)
		return(response)

	def get_activation_key_id(self, organization_name, activation_key_name):
		response = self.list_activation_keys(organization_name)
		activation_keys = response.json()['results']
		activation_key_id = 0
		for activation_key in activation_keys:
			if (activation_key_name == activation_key['name']):
				activation_key_id = activation_key['id']
		return(activation_key_id)

	def list_activation_keys(self, organization_name):
		organization_id = self.get_organization_id(organization_name)
		response = self._Satellite_API.katello_api_organizations_activation_keys_get(organization_id)
		response = self.wait_for_completion(response)
		return(response)

	def override_activation_key_content(self, organization_name, activation_key_name, content_label, value, name, remove=False):
		activation_key_id = self.get_activation_key_id(organization_name, activation_key_name)
		response = self._Satellite_API.katello_api_activation_keys_content_override_put(activation_key_id, content_label, value, name, remove)
		response = self.wait_for_completion(response)
		return(response)


	#---------------------------------------------------------------------------
	#	Composite Content Views
	#---------------------------------------------------------------------------
	def add_components_to_composite_content_view(self, organization_name, composite_content_view_name, component_names):
		composite_content_view_id = self.get_content_view_id(organization_name, composite_content_view_name)
		components = {}
		for component_name in component_names:
			component_id = self.get_content_view_id(organization_name, component_name)
			component_version_id = self.get_latest_content_view_version_id(organization_name, component_name)
			components[component_id] = component_version_id
		response = self._Satellite_API.katello_api_content_views_content_view_components_add_put(composite_content_view_id, components)
		response = self.wait_for_completion(response)
		return(True)

	def get_composite_view_component_id(self, organization_name, composite_content_view_name, component_content_view_name):
		organization_id = self.get_organization_id(organization_name)
		composite_content_view_id = self.get_content_view_id(organization_name, composite_content_view_name)
		response = self._Satellite_API.katello_api_content_views_content_view_components_get(composite_content_view_id)
		component_id = 0
		components = response.json()['results']
		for component in components:
			if (component['content_view']['name'] == component_content_view_name):
				component_content_view_id = component['id']
				break
		return(component_content_view_id)

	def list_composite_content_view_components(self, organization_name, composite_content_view_name):
		composite_content_view_id = self.get_content_view_id(organization_name, composite_content_view_name)
		response = self._Satellite_API.katello_api_content_views_content_view_components_get(composite_content_view_id)
		component_content_views = []
		content_views = response.json()['results']
		for content_view in content_views:
			component_content_views.append(content_view['content_view']['name'])
		return(component_content_views)

	def update_component_content_view_to_latest_version(self, organization_name, composite_content_view_name, component_content_view_name):
		composite_content_view_id = self.get_content_view_id(organization_name, composite_content_view_name)
		component_id = self.get_composite_view_component_id(organization_name, composite_content_view_name, component_content_view_name)
		component_version_id = self.get_latest_content_view_version_id(organization_name, component_content_view_name)
		response = self._Satellite_API.katello_api_content_views_content_view_components_put(composite_content_view_id, component_id, component_version_id)
		response = self.wait_for_completion(response)
		return(response)


	#---------------------------------------------------------------------------
	#	Content Views
	#---------------------------------------------------------------------------
	def content_view_exists(self, organization_name, content_view_name):
		response = self.list_content_views(organization_name)
		content_views = response.json()['results']
		exists = False
		for content_view in content_views:
			if (content_view_name == content_view['name']):
				exists = True
				break
		return(exists)

	def create_content_view(self, content_view_name, content_view_label, organization_name, composite=False, description='', repository_names=[], component_names=[]):
		return_code = False
		if not self.content_view_exists(organization_name, content_view_name):
			print("content view " + content_view_name + " does not exist, creating ...")
			organization_id = self.get_organization_id(organization_name)
			if (composite):
				component_ids = []
				for component_name in component_names:
					component_id = self.get_component_id(component_name)
					component_ids.append(int(component_id))
				response = self._Satellite_API.katello_api_organizations_content_views_post(content_view_name, content_view_label, organization_id, composite, description=description, component_ids=component_ids)
			else:
				repository_ids = []
				for repository_name in repository_names:
					repository_id = self.get_repository_id(organization_name, repository_name)
					repository_ids.append(int(repository_id))
				response = self._Satellite_API.katello_api_organizations_content_views_post(content_view_name, content_view_label, organization_id, composite, description=description, repository_ids=repository_ids)
			response = self.wait_for_completion(response)
			return_code = True
		return(return_code)

	def get_content_view(self, organization_name, content_view_name):
		response = self.list_content_views(organization_name)
		content_views = response.json()['results']
		content_view_output = None
		for content_view in content_views:
			if (content_view_name == content_view['name']):
				content_view_output = content_view
		return(content_view_output)

	def get_content_view_id(self, organization_name, content_view_name):
		response = self.list_content_views(organization_name)
		content_views = response.json()['results']
		content_view_id = 0
		for content_view in content_views:
			if (content_view_name == content_view['name']):
				content_view_id = content_view['id']
		return(content_view_id)

	def get_latest_content_view_version_id(self, organization_name, content_view_name):
		organization_id = self.get_organization_id(organization_name)
		response = self.list_content_view_versions(organization_name, content_view_name)
		content_views = response.json()['results']
		major = 0
		minor = 0
		content_view_id = 0
		for content_view in content_views:
			if (content_view['major'] > major):
				major = content_view['major']
				content_view_id = content_view['id']
			elif (content_view['major'] == major and content_view['minor'] > minor):
				minor = content_view['minor']
				content_view_id = content_view['id']
		return(content_view_id)

	def list_content_view_versions(self, organization_name, content_view_name):
		organization_id = self.get_organization_id(organization_name)
		content_view_id = self.get_content_view_id(organization_name, content_view_name)
		response = self._Satellite_API.katello_api_content_views_content_view_versions_get(organization_id, content_view_id)
		response = self.wait_for_completion(response)
		return(response)

	def list_content_views(self, organization_name):
		organization_id = self.get_organization_id(organization_name)
		response = self._Satellite_API.katello_api_organizations_content_views_get(organization_id)
		response = self.wait_for_completion(response)
		return(response)

	def new_content_view(self, organization_name, content_view_name):
		new = False
		response = self.list_content_view_versions(organization_name, content_view_name)
		if (response.json()['total'] == 0):
			new = True
		return(new)

	def promote_latest_content_view_version_to_lifecycle_environment(self, organization_name, content_view_name, lifecycle_environment_name):
		organization_id = self.get_organization_id(organization_name)
		content_view_id = self.get_latest_content_view_version_id(organization_name, content_view_name)
		environment_id = self.get_lifecycle_environment_id(organization_name, lifecycle_environment_name)
		environment_ids = []
		environment_ids.append(environment_id)
		response = self._Satellite_API.katello_api_content_view_versions_promote_post(content_view_id, environment_ids, description='')
		response = self.wait_for_completion(response)
		return(response)

	def publish_content_view(self, organization_name, content_view_name, description='', only_if_new=True):
		return_code = False
		new = self.new_content_view(organization_name, content_view_name)
		if ((only_if_new and new) or (not only_if_new)):
			content_view_id = self.get_content_view_id(organization_name, content_view_name)
			uri = "katello/api/content_views/" + str(content_view_id) + "/publish"
			response = self._Satellite_API.katello_api_content_views_publish_post(content_view_id, description)
			response = self.wait_for_completion(response)
			return_code = True
		return(return_code)


	#---------------------------------------------------------------------------
	#	Foreman Tasks
	#---------------------------------------------------------------------------
	def get_task_status(self, response):
		task_status = ""
		if ('id' in response.json()):
			task_id = response.json()['id']
			task_status = self._Satellite_API.foreman_tasks_api_tasks(task_id)
		return(task_status)

	def wait_for_completion(self, response):
		task_status = response
		if (('id' in response.json()) and ('result' in response.json())):
			task_id = response.json()['id']
			result = "pending"
			while (result == "pending"):
				time.sleep(1)
				task_status = self._Satellite_API.foreman_tasks_api_tasks(task_id)
				result = task_status.json()['result']
				progress = task_status.json()['progress']
				percentage = str(int((progress * 100)))
				sys.stdout.write("             \r")
				sys.stdout.write("progress: " + percentage + "%")
				sys.stdout.flush()
				if (progress == 1.0):
					sys.stdout.write("              \r")
					sys.stdout.flush()
		return(task_status)


	#---------------------------------------------------------------------------
	#	Lifecycle Environments
	#---------------------------------------------------------------------------
	def create_lifecycle_environment(self, organization_name, lifecycle_environment_name, lifecycle_environment_label, prior_lifecycle_environment_name, description=''):
		response = None
		if not self.lifecycle_environment_exists(organization_name, lifecycle_environment_name):
			print("lifecycle environment " + lifecycle_environment_name + " does not exist, creating ...")
			organization_id = self.get_organization_id(organization_name)
			prior_id = self.get_lifecycle_environment_id(organization_name, prior_lifecycle_environment_name)
			response = self._Satellite_API.katello_api_organizations_environments_post(organization_id, lifecycle_environment_name, lifecycle_environment_label, prior_id, description)
		response = self.wait_for_completion(response)
		return(response)

	def get_lifecycle_environment_id(self, organization_name, lifecycle_environment_name):
		response = self.list_lifecycle_environments(organization_name)
		lifecycle_environments = response.json()['results']
		lifecycle_environment_id = 0
		for lifecycle_environment in lifecycle_environments:
			if (lifecycle_environment_name == lifecycle_environment['name']):
				lifecycle_environment_id = lifecycle_environment['id']
		return(lifecycle_environment_id)

	def get_lifecycle_environments(self, organization_name):
		lifecycle_environments = []
		response = self.list_lifecycle_environments(organization_name)
		for lifecycle_environment_entry in response.json()['results']:
			entry_name = lifecycle_environment_entry['name']
			if (not lifecycle_environment_entry['prior']):
				# no prior implies Library which goes at the front of the list
				lifecycle_environments.insert(0, entry_name)
			elif ((len(lifecycle_environments) == 0) or \
				(not lifecycle_environment_entry['successor'])):
				# nothing in the list yet or no successor
				# goes at the end of the list
				lifecycle_environments.append(entry_name)
			else:
				# search for the successor in the list, insert before
				successor = lifecycle_environment_entry['successor']['name']
				found = 0
				for i in range(len(lifecycle_environments)):
					if (lifecycle_environments[i] == successor):
						lifecycle_environments.insert(i, entry_name)
						found = 1
						break
				# if successor not found in the list,
				# put entry at the end of the list
				if (not found):
					lifecycle_environments.append(entry_name)
		return(lifecycle_environments)

	def lifecycle_environment_exists(self, organization_name, lifecycle_environment_name):
		response = self.list_lifecycle_environments(organization_name)
		lifecycle_environments = response.json()['results']
		exists = False
		for lifecycle_environment in lifecycle_environments:
			if (lifecycle_environment_name == lifecycle_environment['name']):
				exists = True
				break
		return(exists)

	def list_lifecycle_environments(self, organization_name):
		organization_id = self.get_organization_id(organization_name)
		response = self._Satellite_API.katello_api_organizations_environments_get(organization_id)
		response = self.wait_for_completion(response)
		return(response)


	#---------------------------------------------------------------------------
	#	Locations
	#---------------------------------------------------------------------------
	def create_location(self, location_name, description=''):
		return_code = False
		if not self.location_exists(location_name):
			print("location " + location_name + " does not exist, creating ...")
			response = self._Satellite_API.api_locations_post( location_name, description)
			response = self.wait_for_completion(response)
			return_code = True
		return(return_code)

	def delete_location(self, location_name):
		location_id = self.get_location_id(location_name)
		response = self._Satellite_API.api_locations_delete(location_id)
		response = self.wait_for_completion(response)
		return(response)

	def get_location_id(self, location_name):
		response = self.list_locations()
		locations = response.json()['results']
		location_id = 0
		for location in locations:
			if (location_name == location['name']):
				location_id = location['id']
		return(location_id)

	def list_locations(self):
		response = self._Satellite_API.api_locations_get()
		response = self.wait_for_completion(response)
		return(response)

	def location_exists(self, location_name):
		response = self.list_locations()
		locations = response.json()['results']
		exists = False
		for location in locations:
			if (location_name == location['name']):
				exists = True
				break
		return(exists)


	#---------------------------------------------------------------------------
	#	Organizations
	#---------------------------------------------------------------------------
	def create_organization(self, organization_name, description=''):
		return_code = False
		if not self.organization_exists(organization_name):
			print("organization " + organization_name + " does not exist, creating ...")
			response = self._Satellite_API.katello_api_organizations_post(organization_name, description)
			response = self.wait_for_completion(response)
			return_code = True
		return(return_code)

	def delete_organization(self, organization_name):
		organization_id = self.get_organization_id(organization_name)
		response = self._Satellite_API.katello_api_organizations_delete(organization_id)
		response = self.wait_for_completion(response)
		return(response)

	def get_organization_id(self, organization_name):
		if (organization_name in self._organization_ids):
			organization_id = self._organization_ids[organization_name]
		else:
			response = self.list_organizations()
			organizations = response.json()['results']
			organization_id = 0
			for organization in organizations:
				if (organization_name == organization['name']):
					organization_id = organization['id']
					self._organization_ids[organization_name] = organization_id
					break
		return(organization_id)

	def list_organizations(self):
		response = self._Satellite_API.katello_api_organizations_get()
		response = self.wait_for_completion(response)
		return(response)

	def organization_exists(self, organizaton_name):
		response = self.list_organizations()
		organizations = response.json()['results']
		exists = False
		for organization in organizations:
			if (organizaton_name == organization['name']):
				exists = True
				break
		return(exists)


	#---------------------------------------------------------------------------
	#	Products
	#---------------------------------------------------------------------------
	def get_product_id(self, organization_name, product_name):
		response = self.list_products(organization_name)
		products = response.json()['results']
		product_id = 0
		for product in products:
			if (product_name == product['name']):
				product_id = product['id']
		return(product_id)

	def list_products(self, organization):
		organization_id = self.get_organization_id(organization)
		response = self._Satellite_API.katello_api_organizations_products_get(organization_id)
		response = self.wait_for_completion(response)
		return(response)


	#---------------------------------------------------------------------------
	#	Repositories
	#---------------------------------------------------------------------------
	def enable_repository(self, organization_name, product_name, repository_name, releasever):
		return_code = True
		organization_id = self.get_organization_id(organization_name)
		product_id = self.get_product_id(organization_name, product_name)
		repository_set_id = self.get_repository_set_id(organization_name, product_name, repository_name)
		response = self._Satellite_API.katello_api_products_repository_sets_enable_put(organization_id, product_id, repository_set_id, releasever)
		response = self.wait_for_completion(response)
		return(return_code)

	def get_repository_id(self, organization_name, repository_name):
		response = self.list_repositories(organization_name)
		repositories = response.json()['results']
		repository_id = 0
		for repository in repositories:
			if (repository_name == repository['name']):
				repository_id = repository['id']
		return(repository_id)

	def list_repositories(self, organization_name):
		organization_id = self.get_organization_id(organization_name)
		response = self._Satellite_API.katello_api_organizations_repositories_get(organization_id)
		response = self.wait_for_completion(response)
		return(response)


	#---------------------------------------------------------------------------
	#	Repository Sets
	#---------------------------------------------------------------------------
	def get_repository_set_id(self, organization_name, product_name, repository_name):
		response = self.list_repository_sets(organization_name, product_name)
		repository_sets = response.json()['results']
		content_id = 0
		for repository_set in repository_sets:
			if (repository_name == repository_set['name']):
				repository_set_id = repository_set['id']
		return(repository_set_id)

	def list_repository_sets(self, organization_name, product_name):
		organization_id = self.get_organization_id(organization_name)
		product_id = self.get_product_id(organization_name, product_name)
		response = self._Satellite_API.katello_api_products_repository_sets_get(organization_id, product_id)
		response = self.wait_for_completion(response)
		return(response)


	#---------------------------------------------------------------------------
	#	Subscriptions
	#---------------------------------------------------------------------------
	def list_subscriptions(self, organization_name):
		organization_id = self.get_organization_id(organization_name)
		response = self._Satellite_API.katello_api_organizations_subscriptions_get(organization_id)
		response = self.wait_for_completion(response)
		return(response)

	def refresh_subscription_manifest(self, organization_name):
		organization_id = self.get_organization_id(organization_name)
		response = self._Satellite_API.katello_api_organizations_subscriptions_refresh_manifest_put(organization_id)
		response = self.wait_for_completion(response)
		return(True)

	def subscription_exists(self, organization_name):
		response = self.list_subscriptions(organization_name)
		exists = False
		if (response.json()['total'] > 0):
			exists = True
		return(exists)

	def upload_subscription_manifest(self, organization_name, manifest_file_name):
		return_code = False
		if (not self.subscription_exists(organization_name)):
			organization_id = self.get_organization_id(organization_name)
			response = self._Satellite_API.katello_api_organizations_subscriptions_upload_post(organization_id, manifest_file_name)
			response = self.wait_for_completion(response)
			return_code = True
		return(return_code)


	#---------------------------------------------------------------------------
	#	Sync Plans
	#---------------------------------------------------------------------------
	def add_products_to_sync_plan(self, organization_name, sync_plan_name, product_names):
		organization_id = self.get_organization_id(organization_name)
		sync_plan_id = self.get_sync_plan_id(organization_name, sync_plan_name)
		product_ids = []
		for product_name in product_names:
			product_id = self.get_product_id(organization_name, product_name)
			product_ids.append(product_id)
		response = self._Satellite_API.katello_api_organizations_sync_plans_add_products_put(organization_id, sync_plan_id, product_ids)
		response = self.wait_for_completion(response)
		return(response)

	def create_sync_plan(self, organization_name, sync_plan_name, interval, sync_date_time):
		return_code = False
		if not self.sync_plan_exists(organization_name, sync_plan_name):
			print("sync plan " + sync_plan_name + " does not exist, creating ...")
			organization_id = self.get_organization_id(organization_name)
			response = self._Satellite_API.katello_api_organizations_sync_plans_post(organization_id, sync_plan_name, interval, sync_date_time)
			response = self.wait_for_completion(response)
			return_code = True
		return(return_code)

	def get_sync_plan_id(self, organization_name, sync_plan_name):
		response = self.list_sync_plans(organization_name)
		sync_plans = response.json()['results']
		sync_plan_id = 0
		for sync_plan in sync_plans:
			if (sync_plan_name == sync_plan['name']):
				sync_plan_id = sync_plan['id']
		return(sync_plan_id)

	def list_sync_plans(self, organization_name):
		organization_id = self.get_organization_id(organization_name)
		response = self._Satellite_API.katello_api_organizations_sync_plans_get(organization_id)
		response = self.wait_for_completion(response)
		return(response)

	def sync_plan_exists(self, organization_name, sync_plan_name):
		response = self.list_sync_plans(organization_name)
		sync_plans = response.json()['results']
		exists = False
		for sync_plan in sync_plans:
			if (sync_plan_name == sync_plan['name']):
				exists = True
				break
		return(exists)


